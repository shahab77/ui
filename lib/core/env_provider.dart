import 'dart:convert';

import 'package:flutter/services.dart' show rootBundle;

class EnvProvider {
  static late int SPACE_TINY;
  static late int SPACE_NORMAL;
  static late int SPACE_LARGE;
  static late int SPACE_EXTRA_LARGE;
  static late int RADIUS_TINY;
  static late int RADIUS_NORMAL;
  static late int RADIUS_LARGE;

  static Future<String> _loadAsset(String path) {
    return rootBundle.loadString("assets/env/$path.json");
  }

  static Future<Map<String, dynamic>> _provideIntegers() async {
    String json = await _loadAsset("integers");
    return jsonDecode(json);
  }

  static Future<void> init() async {
    final integers = await _provideIntegers();
    SPACE_TINY = integers['space_tiny'];
    SPACE_NORMAL = integers['space_normal'];
    SPACE_LARGE = integers['space_large'];
    SPACE_EXTRA_LARGE = integers['space_extra_large'];
    RADIUS_TINY = integers['radius_tiny'];
    RADIUS_NORMAL = integers['radius_normal'];
    RADIUS_LARGE = integers['radius_large'];
  }
}
