import 'package:flutter/widgets.dart';
import '../utill/native_management.dart';

class NativeData {
  final bool isAndroid;
  final bool isIOS;
  final bool isWindows;
  final bool isWeb;

  NativeData(
      {required this.isAndroid,
      required this.isIOS,
      required this.isWindows,
      required this.isWeb});
}

class NativeWidgetBuilder extends StatelessWidget {
  const NativeWidgetBuilder({Key? key, required this.builder})
      : super(key: key);
  final Widget Function(BuildContext context, NativeData data) builder;

  @override
  Widget build(BuildContext context) {
    return builder(
        context,
        NativeData(
          isAndroid: isAndroid(),
          isIOS: isIOS(),
          isWeb: !isNative(),
          isWindows: isWindows(),
        ));
  }
}
