import 'package:blur/blur.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../ui/native_widget_builder.dart';

class BaseScaffold<T> extends StatelessWidget {
  final AppBar? appBar;
  final Widget body;
  final Map<T, Widget> cupertinoSlideSegmentChildren;
  final void Function(T? value)? onValueChanged;
  final T? groupValue;
  final Color? backgroundColor;
  final Widget? drawer;
  final Widget? bottomNavigationBar;
  final Widget? floatingActionButton;

  const BaseScaffold({
    Key? key,
    required this.body,
    this.appBar,
    this.cupertinoSlideSegmentChildren = const {},
    this.onValueChanged,
    this.groupValue,
    this.backgroundColor,
    this.drawer,
    this.bottomNavigationBar,
    this.floatingActionButton,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NativeWidgetBuilder(builder: (context, data) {
      return data.isIOS
          ? CupertinoPageScaffold(
              navigationBar: CupertinoNavigationBar(
                middle: CupertinoSlidingSegmentedControl<T>(
                    groupValue: groupValue,
                    children: cupertinoSlideSegmentChildren,
                    onValueChanged: onValueChanged ?? (value) {}),
              ),
              child: body,
            )
          : Scaffold(
              appBar: data.isWindows ? null : appBar,
              body:body,
              floatingActionButton: floatingActionButton,
              bottomNavigationBar: bottomNavigationBar,
              backgroundColor: Colors.transparent,
              drawer: drawer,
            );
    });
  }
}
