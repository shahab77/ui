import 'package:flutter/cupertino.dart';
import 'package:responsive_framework/responsive_framework.dart';

class DeviceData {
  final bool isDes;
  final bool isTab;
  final bool isPhone;
  final double screenWidth;
  final double screenHeight;
  final Orientation orientation;
  final ResponsiveWrapperData wrapperData;

  DeviceData(
    this.isDes,
    this.isTab,
    this.isPhone,
    this.screenWidth,
    this.screenHeight,
    this.orientation,
    this.wrapperData,
  );
}

class ResponsiveBuilder extends StatelessWidget {
  final Widget Function(
    BuildContext context,
    DeviceData,
  ) builder;

  const ResponsiveBuilder({
    Key? key,
    required this.builder,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final data = DeviceData(
      ResponsiveWrapper.of(context).isDesktop,
      ResponsiveWrapper.of(context).isTablet,
      ResponsiveWrapper.of(context).isPhone,
      ResponsiveWrapper.of(context).screenWidth,
      ResponsiveWrapper.of(context).screenHeight,
      ResponsiveWrapper.of(context).orientation,
      ResponsiveWrapper.of(context),
    );
    return builder(
      context,
      data,
    );
  }
}
