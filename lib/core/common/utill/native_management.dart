import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

bool isNative() => !kIsWeb;

bool isAndroid() =>
    isNative() && defaultTargetPlatform == TargetPlatform.android;

bool isIOS() => isNative() && defaultTargetPlatform == TargetPlatform.iOS;

bool isWindows() =>
    isNative() && defaultTargetPlatform == TargetPlatform.windows;

Future<T?> navigateTo<T>({
  required BuildContext context,
  required Widget distance,
  Object? args,
  bool root=false
}) {
  if (isIOS()) {
   return Navigator.of(context,rootNavigator: root).push(
      CupertinoPageRoute<T>(
        builder: (context) {
          return distance;
        },
        settings: RouteSettings(
          arguments: args,
        ),
      ),
    );
  } else {
   return Navigator.of(context,rootNavigator: root).push(
      MaterialPageRoute<T>(
        builder: (context) {
          return distance;
        },
        settings: RouteSettings(
          arguments: args,
        ),
      ),
    );
  }
}


Future<T?> navigateToReplace<T>({
  required BuildContext context,
  required Widget distance,
  Object? args,
  bool root=false
}) {
  if (isIOS()) {
   return Navigator.of(context,rootNavigator: root).pushReplacement(
      CupertinoPageRoute<T>(
        builder: (context) {
          return distance;
        },
        settings: RouteSettings(
          arguments: args,
        ),
      ),
    );
  } else {
    return Navigator.of(context,rootNavigator: root).pushReplacement(
      MaterialPageRoute<T>(
        builder: (context) {
          return distance;
        },
        settings: RouteSettings(
          arguments: args,
        ),
      ),
    );
  }
}
