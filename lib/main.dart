import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:ui/core/env_provider.dart';
import 'package:ui/feature/home/presentation/page/home_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EnvProvider.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);
  bool isDark = true;
  final purple = Colors.deepPurple;
  final purpleAccent = Colors.purple;
  final white = Colors.white;
  final gray_50 = Colors.grey[50];
  final gray_200 = Colors.grey[200];
  final gray_300 = Colors.grey[300];
  final gray_850 = Colors.grey[850];
  final gray_900 = Colors.grey[900];

  ColorScheme _buildColorScheme(ThemeData base) {
    return base.colorScheme.copyWith(
      brightness: isDark ? Brightness.dark : Brightness.light,
      primary: purple,
      primaryContainer: purpleAccent,
      background: isDark ? gray_900 : gray_200,
      surface: isDark ? gray_850 : white,
      onSurface: isDark ? gray_200 : gray_850,
      onBackground: isDark ? gray_200 : gray_900,
    );
  }

  ThemeData _buildThemeData(BuildContext context) {
    final base = ThemeData(
        brightness: isDark ? Brightness.dark : Brightness.light,
        primaryColor: purple,
        fontFamily: 'Yekan');
    final colorScheme = _buildColorScheme(base);
    return base.copyWith(
      tabBarTheme: Theme.of(context).tabBarTheme.copyWith(
          labelColor: colorScheme.onSurface,
          labelPadding: EdgeInsets.all(
            EnvProvider.SPACE_TINY.toDouble(),
          ),
          indicator: UnderlineTabIndicator(
            borderSide: BorderSide(
              width: 4,
              color: colorScheme.primaryContainer.withOpacity(.7),
            ),
          )),
      appBarTheme: Theme.of(context).appBarTheme.copyWith(
            backgroundColor: Colors.transparent,
            titleTextStyle: Theme.of(context)
                .textTheme
                .bodyLarge
                ?.copyWith(fontFamily: "Yekan"),
            foregroundColor: colorScheme.onSurface,
          ),
      colorScheme: colorScheme,
      backgroundColor: isDark ? gray_900 : gray_200,
      scaffoldBackgroundColor: isDark ? gray_900 : gray_200,
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: _buildThemeData(context),
      debugShowCheckedModeBanner: false,
      builder: (context, child) {
        return ResponsiveWrapper.builder(
          child,
          maxWidth: 1400,
          minWidth: 480,
          backgroundColor: gray_200,
          defaultScale: true,
          breakpoints: const [
            ResponsiveBreakpoint.resize(480, name: MOBILE),
            ResponsiveBreakpoint.resize(530, name: TABLET),
            ResponsiveBreakpoint.resize(1000, name: DESKTOP),
          ],
        );
      },
      home: HomePage(),
    );
  }
}
