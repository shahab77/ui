class Image {
  final String title;
  final String imageUrl;
  final String subTitle;

  Image({
    required this.title,
    required this.imageUrl,
    required this.subTitle,
  });
}
