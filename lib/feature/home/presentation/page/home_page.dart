import 'package:blur/blur.dart';
import 'package:flutter/material.dart';
import 'package:ui/core/common/ui/base_scaffold.dart';
import 'package:ui/core/env_provider.dart';
import 'package:ui/feature/home/presentation/page/first_list_page.dart';
import 'package:ui/feature/home/presentation/page/image_page.dart';
import 'package:ui/feature/home/presentation/widget/home_image_list_widget.dart';
import 'package:ui/feature/home/presentation/widget/search_button.dart';

class HomePage extends StatelessWidget {
  final navigatorKey=GlobalKey<NavigatorState>();
  final tabs = [
    "Recommendation",
    "Popular",
    "New musicians",
    "Hip hop",
    "Blue"
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 5,
      initialIndex: 0,
      child: BaseScaffold(
        body: SafeArea(
          child: Container(
            decoration: const BoxDecoration(
              gradient: RadialGradient(colors: [
                Color(0xff6c1e79),
                Color(0xff241552),
                Color(0xff150e34),
              ], radius: 1, center: Alignment(-.9, .2)
                  // transform: const GradientRotation(20),
                  ),
            ),
            child: WillPopScope(
              onWillPop:()async {
                return ! await navigatorKey.currentState!.maybePop();
              },
              child: Navigator(
                key:navigatorKey,
                initialRoute: '/',
                onGenerateRoute: (settings) {
                  Widget? page;
                  switch (settings.name) {
                    case "/":
                      page = Blur(
                        borderRadius: BorderRadius.circular(1.5),
                        blur: 25,
                        blurColor: Colors.transparent,
                        colorOpacity: 0,
                        overlay: Stack(
                          children: [
                            NestedScrollView(
                              headerSliverBuilder: (context, child) {
                                return [
                                  const SliverToBoxAdapter(
                                    child: SizedBox(
                                      height: 100,
                                    ),
                                  ),
                                  SliverAppBar(
                                    expandedHeight: 360,
                                    flexibleSpace: HomeImageListWidget(),
                                  ),
                                ];
                              },
                              body: Column(
                                children: [
                                  TabBar(
                                    padding: EdgeInsets.only(
                                        left:
                                            EnvProvider.RADIUS_LARGE.toDouble()),
                                    isScrollable: true,
                                    tabs: tabs
                                        .map(
                                          (e) => Tab(
                                            child: Text(
                                              e,
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline6!
                                                  .copyWith(
                                                    color: Theme.of(context)
                                                        .colorScheme
                                                        .onBackground
                                                        .withOpacity(.7),
                                                  ),
                                            ),
                                          ),
                                        )
                                        .toList(),
                                  ),
                                  Expanded(
                                    child: TabBarView(children: [
                                      FirstListPage(),
                                      FirstListPage(),
                                      FirstListPage(),
                                      FirstListPage(),
                                      FirstListPage(),
                                    ]),
                                  ),
                                ],
                              ),
                            ),
                            Positioned(right: 20, top: 20, child: SearchButton()),
                          ],
                        ),
                        child: Container(),
                      );
                      break;
                    case "/image":
                      page = ImagePage();
                      break;
                  }
                  return MaterialPageRoute(
                    builder: (context) {
                      return page!;
                    },
                    settings: settings,
                  );
                },
                observers: [
                  HeroController(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
