import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:ui/core/common/ui/base_scaffold.dart';
import 'package:ui/core/env_provider.dart';
import 'package:ui/feature/home/presentation/widget/image_linear_widget.dart';
import '../../domain/Image.dart' as image;

class FirstListPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final items = <image.Image>[];
    items.add(image.Image(
        title: "Clear mind",
        imageUrl: "assets/images/1.jpg",
        subTitle: "instrumental"));
    items.add(image.Image(
        title: "Urban mood",
        imageUrl: "assets/images/2.jpg",
        subTitle: "Hip-hop"));
    items.add(image.Image(
        title: "Summer day",
        imageUrl: "assets/images/3.jpg",
        subTitle: "Club music"));
    return BaseScaffold(
        body: GridView(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisSpacing: EnvProvider.SPACE_LARGE.toDouble(),
        mainAxisExtent: 150,
        crossAxisCount: ResponsiveValue<int>(
          context,
          defaultValue: 1,
          valueWhen: const [
            Condition.largerThan(name: MOBILE,value: 2),
            Condition.largerThan(name: TABLET,value: 3),
          ],
        ).value!,
      ),
      padding: EdgeInsets.symmetric(
        horizontal: EnvProvider.SPACE_NORMAL.toDouble(),
        vertical: EnvProvider.SPACE_NORMAL.toDouble(),
      ),
      children: List.generate(10, (index) {
        final item = items[index % 3];
        return ImageLinearWidget(
            imageUrl: item.imageUrl,
            title: item.title,
            subTitle: item.subTitle);
      }),
    ));
  }
}
