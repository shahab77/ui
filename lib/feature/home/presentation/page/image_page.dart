import 'package:flutter/material.dart';
import 'package:responsive_framework/responsive_framework.dart';
import 'package:ui/core/common/ui/base_scaffold.dart';
import 'package:ui/core/common/ui/responsive_builder.dart';
import 'package:ui/core/env_provider.dart';
import 'package:ui/feature/home/presentation/widget/image_linear_widget.dart';
import '../../domain/Image.dart' as image;

class ImagePage extends StatelessWidget {
  final controller = ScrollController();

  @override
  Widget build(BuildContext context) {
    final imageModel =
    ModalRoute
        .of(context)!
        .settings
        .arguments as image.Image;
    final items = <image.Image>[];
    items.add(image.Image(
        title: "Clear mind",
        imageUrl: "assets/images/1.jpg",
        subTitle: "instrumental"));
    items.add(image.Image(
        title: "Urban mood",
        imageUrl: "assets/images/2.jpg",
        subTitle: "Hip-hop"));
    items.add(image.Image(
        title: "Summer day",
        imageUrl: "assets/images/3.jpg",
        subTitle: "Club music"));
    return WillPopScope(
      onWillPop: () async {
        controller.animateTo(0, duration: const Duration(milliseconds: 300), curve: Curves.easeOutCubic).then((value) {
          Navigator.of(context).pop();
        });
        return controller.offset==0;
      },
      child: BaseScaffold(
        body: ResponsiveBuilder(builder: (context, data) {
          return CustomScrollView(
            controller: controller,
            slivers: [
              SliverAppBar(
                expandedHeight: 400,
                title: data.isPhone
                    ? Text(
                  imageModel.title,
                  style: Theme
                      .of(context)
                      .textTheme
                      .headline6!
                      .copyWith(
                      color: Theme
                          .of(context)
                          .colorScheme
                          .onBackground),
                )
                    : null,
                flexibleSpace: Stack(
                  clipBehavior: Clip.none,
                  children: [
                    if (data.wrapperData.isLargerThan(MOBILE))
                      Positioned(
                          left: 70,
                          bottom: 30,
                          child: Row(
                            children: [
                              SizedBox(
                                width: 200,
                                height: 250,
                                child: Hero(
                                  tag: imageModel.imageUrl,
                                  child: Transform.scale(
                                    scale: 1.1,
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(
                                          EnvProvider.RADIUS_LARGE.toDouble()),
                                      child: Image.asset(
                                        imageModel.imageUrl,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: EnvProvider.SPACE_LARGE.toDouble(),
                              ),
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    imageModel.title,
                                    style: Theme
                                        .of(context)
                                        .textTheme
                                        .headline6,
                                  ),
                                  SizedBox(
                                    height: EnvProvider.SPACE_LARGE.toDouble(),
                                  ),
                                  Text(
                                    imageModel.subTitle,
                                    style: Theme
                                        .of(context)
                                        .textTheme
                                        .bodyLarge,
                                  ),
                                ],
                              )
                            ],
                          ))
                    else
                      Positioned.fill(
                        child: Hero(
                          tag: imageModel.imageUrl,
                          child: Transform.scale(
                            scale: 1.1,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(
                                  EnvProvider.RADIUS_LARGE.toDouble()),
                              child: Image.asset(
                                imageModel.imageUrl,
                                fit: BoxFit.cover,
                              ),
                            ),
                          ),
                        ),
                      )
                  ],
                ),
              ),
              SliverPadding(
                padding: EdgeInsets.all(EnvProvider.SPACE_LARGE.toDouble()),
                sliver: SliverGrid(
                  delegate: SliverChildListDelegate(
                    List.generate(
                      10,
                          (index) {
                        final item = items[index % 3];
                        return ImageLinearWidget(
                            imageUrl: item.imageUrl,
                            title: item.title,
                            subTitle: item.subTitle);
                      },
                    ),
                  ),
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisSpacing: EnvProvider.SPACE_LARGE.toDouble(),
                    mainAxisExtent: 150,
                    crossAxisCount: ResponsiveValue<int>(
                      context,
                      defaultValue: 1,
                      valueWhen: const [
                        Condition.largerThan(name: MOBILE, value: 2),
                        Condition.largerThan(name: TABLET, value: 3),
                      ],
                    ).value!,
                  ),
                  // padding: EdgeInsets.symmetric(
                  // horizontal: EnvProvider.SPACE_NORMAL.toDouble(),
                  // vertical: EnvProvider.SPACE_NORMAL.toDouble(),
                  // ),
                ),
              )
            ],
          );
        }),
      ),
    );
  }
}
