import 'package:flutter/material.dart';
import 'package:ui/core/env_provider.dart';
import 'package:ui/feature/home/presentation/widget/image_widget.dart';
import '../../domain/Image.dart' as image;

class HomeImageListWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final items = <image.Image>[];
    items.add(image.Image(title: "Clear mind",
        imageUrl: "assets/images/1.jpg",
        subTitle: "instrumental"));
    items.add(image.Image(title: "Urban mood",
        imageUrl: "assets/images/2.jpg",
        subTitle: "Hip-hop"));
    items.add(image.Image(title: "Summer day",
        imageUrl: "assets/images/3.jpg",
        subTitle: "Club music"));
    return SingleChildScrollView(
      physics: const NeverScrollableScrollPhysics(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: EdgeInsets.only(left: EnvProvider.SPACE_NORMAL.toDouble()),
            child: Text('Browse',style: Theme.of(context).textTheme.headline3,),
          ),
          SizedBox(
            height: 320,
            child: ListView(
              padding: EdgeInsets.symmetric(vertical: EnvProvider.SPACE_NORMAL.toDouble(),horizontal: EnvProvider.SPACE_NORMAL.toDouble()),
              scrollDirection: Axis.horizontal,
              children: items.map((e) =>
                  GestureDetector(
                    onTap: (){
                      Navigator.of(context).pushNamed("/image",arguments: e);
                    },
                    child: ImageWidget(
                        imageUrl: e.imageUrl,
                        title: e.title,
                        subTitle: e.subTitle),
                  ),)
                  .toList(),
            ),
          ),
        ],
      ),
    );
  }
}
