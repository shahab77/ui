import 'package:flutter/material.dart';
import 'package:ui/core/env_provider.dart';

class SearchButton extends StatefulWidget {
  @override
  State<SearchButton> createState() => _SearchButtonState();
}

class _SearchButtonState extends State<SearchButton> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){},
      child: AnimatedContainer(
        width: 80,
        height: 80,
        decoration:  BoxDecoration(
          borderRadius: BorderRadius.circular(100),
            gradient: const LinearGradient(
              colors: [
                Color(0xff150e34),
                Color(0xff241552),
              ],
              transform: GradientRotation(1),
            ),
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(.2),
                offset: Offset(0, 0),
                blurRadius: 30,
                spreadRadius: 1
              )
            ]),
        duration: const Duration(milliseconds: 300),
        child: Center(
          child: Icon(
            Icons.search,
          ),
        ),
      ),
    );
  }
}
