import 'package:flutter/material.dart';
import 'package:ui/core/env_provider.dart';

class ImageLinearWidget extends StatelessWidget {
  final String imageUrl;
  final String title;
  final String subTitle;

  const ImageLinearWidget({
    super.key,
    required this.imageUrl,
    required this.title,
    required this.subTitle,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 30,),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
            width: 100,
            height: 100,
            clipBehavior: Clip.antiAlias,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(
                EnvProvider.RADIUS_LARGE.toDouble(),
              ),
            ),
            child: Image.asset(
              imageUrl,
              fit: BoxFit.cover,
            ),
          ),
          SizedBox(width: EnvProvider.SPACE_NORMAL.toDouble(),),
          Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(title,style: Theme.of(context).textTheme.headline6,),
                SizedBox(height: EnvProvider.SPACE_TINY.toDouble(),),
                Text(subTitle,style: Theme.of(context).textTheme.bodyLarge,),
              ],
            ),
          ),
          Icon(Icons.more_horiz)
        ],
      ),
    );
  }
}
