import 'package:flutter/material.dart';
import 'package:ui/core/env_provider.dart';

class ImageWidget extends StatelessWidget {
  final String imageUrl;
  final String title;
  final String subTitle;

  const ImageWidget({
    super.key,
    required this.imageUrl,
    required this.title,
    required this.subTitle,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: EnvProvider.SPACE_NORMAL.toDouble(),),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Hero(
            tag: imageUrl,
            child: ClipRRect(
              borderRadius:
                  BorderRadius.circular(EnvProvider.RADIUS_LARGE.toDouble()),
              child: SizedBox(
                width: 160,
                height: 190,
                child: Image.asset(imageUrl,fit: BoxFit.cover,),
              ),
            ),
          ),
          SizedBox(height: EnvProvider.SPACE_NORMAL.toDouble(),),
          Text(title,style: Theme.of(context).textTheme.headline6,),
          SizedBox(height: EnvProvider.SPACE_TINY.toDouble(),),
          Text(subTitle, style:Theme.of(context).textTheme.headline6!.copyWith(color: Theme.of(context).colorScheme.onBackground.withOpacity(.5)),),
        ],
      ),
    );
  }
}
